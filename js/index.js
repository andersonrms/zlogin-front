var animation = bodymovin.loadAnimation({
    container: document.getElementById('flex-animation'), // Required
    path: './assets/lf20_tzipHD.json', // Required
    prenderer: 'svg/canvas/html', // Required
    loop: true, // Optional
    autoplay: true, // Optional
  })

function focusedEmail(){
    let className = document.querySelector("div.email-field");
    className.classList.add("email-field-focused");
    className.classList.remove("email-field");
}
